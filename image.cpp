/// OpenCv
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/objdetect/objdetect.hpp>

/// C++
#include <time.h>

#include "maxflow/graph.h"

using namespace cv::ml;
using namespace cv;
using namespace std;

/// Camera idx
int cap_idx = 0;

/// Windows
String input_window = "Hand Input";
String input_result_window = "Hand Input Result";
String control_window = "Graph Cut";
String gray_window = "Movements";
int width = 480, height = 360;

/// Canny parameters
const int lowThreshold = 100;
const int threshRatio = 2;
const int kernel_size = 3;

/// Graph cut colors	
const double norm_color_dist = 18.;						/// Norm distance for colors out of hand
const double norm_hand_dist = 8.;						/// Norm distance for hand colors
vector<Scalar> hand_avgs, out_hand_avgs;

/// Contours & bounding rectangle majoration factor
vector<vector<Point>> hand_contours;
const double br_fact = 1.2;								/// Multiplicative factor for bounding rectangle in which to compute next graph cut

/// Parameters for loss check
Mat1b prev_cuts;
double area_sum = 0.;
const double max_area_fact = 0.4;						/// Factor of total image size for which graph cut is refused
const double br_keep_lim = 1.05;						/// Multiplicative factor for bounding rectangle in which we decide to keep previous colors or not
bool lost = true;
bool keep_hand_avgs = false;

/// Key booleans
bool initiation = false;
bool initialized = false;
bool cut_validated = false;

/// Learning parameters (save)
const Size training_img_size(128, 128);
const int training_sets_count = 5;
String training_sets[training_sets_count] = { "joined", "open", "peace", "fist", "thumb_up" };
String classifier_path = "../classifierModel.yml";

/// Learning parameters (prediction)
bool svm_enabled = true;
const int pred_length = 10;								/// Number of frames for which a new prediction is computed
vector<int> predictions;

/// Movement parameters
const int grid_count = 40;								/// Number of rectangles on which color differences are computed
const double move_size_thresh = grid_count / 10;		/// Minimum count of moving blocks to ensure that there was a movement (movement spatial resolution)
const int move_duration_thresh = 3;						/// Minimum count of frames to ensure that there was a movement (movement temporal resolution)
const double shake_size_thresh = 4;						/// Idem that previous for the specific shake movement (spatial)
const double shake_duration_thresh = 5;					/// Idem that previous for the specific shake movement (temporal)
const float color_thresh = 50;							/// Sensibility to movement: the lower it is, the more sensitive to movement the program becomes
const double momentum_thresh = 5;						/// Sensibility to movement velocity, for point based recognition recommand between 0.7 to 1.2 for history based recommand between 4 and 7 
double hand_color_thresh = 30;							/// Sensibility to hand color in grid color differences computation if used
const double hand_zone_thresh = 3;						/// The higher it is, the smaller the hand zone will be
const double background_thresh = 70;					/// Background persistance
const double bg_update_velocity = 0.2;					/// Background update velocity
//const double angular_thresh = 0.5;
//const int direction_dominance_thresh = 49;

/// Size values
int grid_rows = height / grid_count;
int grid_cols = width / grid_count;



/// POSE ///

// Convert image to YCrCb (commented is a desaturation operation)
void toYCC(const Mat& frame, Mat& ycc_frame) {
	cvtColor(frame, ycc_frame, CV_BGR2YCrCb);
	/*for (int i = 0; i < ycc_frame.rows; i++) {
	for (int j = 0; j < ycc_frame.cols; j++) {
	for (int c = 0; c < 3; c++)
	ycc_frame.at<Vec3b>(i, j)[c] = saturate_cast<uchar>(0.5*(ycc_frame.at<Vec3b>(i, j)[c]));
	}
	}*/
}

// Compute gradient magnitude and orientation
void derivate(const Mat& I, Mat& G, Mat& O)
{
	Mat Gx, Gy;
	Sobel(I, Gx, CV_32F, 1, 0);
	Sobel(I, Gy, CV_32F, 0, 1);
	cartToPolar(Gx, Gy, G, O);
}

double f(double x) {
	return 1 / (1 + x*x);
}

// Returns the lowest norm difference between color and colors in colors_set
double closestColorNorm(const Scalar& color, const vector<Scalar>& colors_set) {
	if (colors_set.empty())
		return 0.;

	double diff = norm(color - colors_set[0]);

	for (Scalar c : colors_set) {
		double ldiff = norm(color - c);
		if (ldiff < diff)
			diff = ldiff;
	}

	return diff;
}

// Computes a graph cut given an image, its gradient and colors sets
Mat1b gCuts(const Mat& I, const Mat& Grad, const vector<Scalar>& hand, const vector<Scalar>& out)
{
	int w = I.cols,
		h = I.rows;

	int nodes_count = w*h;
	Graph<double, double, double> g(nodes_count, 2 * nodes_count);
	g.add_node(nodes_count);
	for (int i = 0; i < w - 1; i++) {
		for (int j = 0; j < h - 1; j++) {
			int node_id = j*w + i;
			Scalar color = I.at<Vec3b>(j, i);
			double dp0 = closestColorNorm(color, hand),
				dp1 = closestColorNorm(color, out);
			g.add_tweights(node_id, dp1, dp0);

			if (i < w - 1) {
				double lambda = (f(Grad.at<float>(j, i)) + f(Grad.at<float>(j, i + 1))) / 2;
				g.add_edge(node_id, node_id + 1, lambda, lambda);
			}
			if (j < h - 1) {
				double lambda = (f(Grad.at<float>(j, i)) + f(Grad.at<float>(j + 1, i))) / 2;
				g.add_edge(node_id, node_id + w, lambda, lambda);
			}
		}
	}

	double flow = g.maxflow();
	Mat1b C = Mat1b::zeros(h, w);
	for (int i = 0; i < w - 1; i++) {
		for (int j = 0; j < h - 1; j++) {
			int node_id = j*w + i;
			if (g.what_segment(node_id) == Graph<double, double, double>::SOURCE)
				C.at<uchar>(j, i) = uchar(255);
		}
	}

	return C;
}

// Same in a region of interest of the image
Mat1b gCutsRoi(const Mat& I, const Mat& Grad, const Rect& roi, const vector<Scalar>& hand, const vector<Scalar>& out)
{
	int w = roi.width,
		h = roi.height;

	int nodes_count = w*h;
	Graph<double, double, double> g(nodes_count, 2 * nodes_count);
	g.add_node(nodes_count);
	for (int i = 0; i < w - 1; i++) {
		for (int j = 0; j < h - 1; j++) {
			int node_id = j*w + i;
			Scalar color = I.at<Vec3b>(roi.y + j, roi.x + i);
			double dp0 = closestColorNorm(color, hand),
				dp1 = closestColorNorm(color, out);
			g.add_tweights(node_id, dp1, dp0);

			double lambda = (f(Grad.at<float>(roi.y + j, roi.x + i)) + f(Grad.at<float>(roi.y + j, roi.x + i + 1))) / 2;
			g.add_edge(node_id, node_id + 1, lambda, lambda);

			lambda = (f(Grad.at<float>(roi.y + j, roi.x + i)) + f(Grad.at<float>(roi.y + j + 1, roi.x + i))) / 2;
			g.add_edge(node_id, node_id + w, lambda, lambda);
		}
	}

	double flow = g.maxflow();
	Mat1b C = Mat1b::zeros(I.rows, I.cols);
	for (int i = 0; i < w - 1; i++) {
		for (int j = 0; j < h - 1; j++) {
			int node_id = j*w + i;
			if (g.what_segment(node_id) == Graph<double, double, double>::SOURCE)
				C.at<uchar>(roi.y + j, roi.x + i) = uchar(255);
		}
	}

	return C;
}

Scalar vecToScalar(const vector<double>& vec) {
	Scalar scal;
	for (int i = 0; i < 3; i++)
		scal[i] = uchar(vec[i]);

	return scal;
}

// Get the dominant color of an image using kmeans
Scalar getDominantColor(Mat& hand) {
	// Get pixels colors as list of samples
	Mat samples(hand.total(), 3, CV_32F);
	float *samples_ptr = samples.ptr<float>(0);
	for (int row = 0; row != hand.rows; ++row) {
		uchar *src_begin = hand.ptr<uchar>(row);
		uchar *src_end = src_begin + hand.cols * hand.channels();
		while (src_begin != src_end) {
			samples_ptr[0] = src_begin[0];
			samples_ptr[1] = src_begin[1];
			samples_ptr[2] = src_begin[2];
			samples_ptr += 3; src_begin += 3;
		}
	}

	// Compute kmeans
	int cluster_count = 5;
	Mat labels;
	int attempts = 5;
	Mat centers;
	kmeans(samples, cluster_count, labels,
		TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS,
			10, 0.01),
		attempts, KMEANS_PP_CENTERS, centers);

	vector<int> colors_count(cluster_count);

	// Count representation of labels
	for (int row = 0; row != hand.rows; row++) {
		int *labels_ptr = labels.ptr<int>(row * hand.cols);
		int *labels_end = labels_ptr + hand.cols;

		while (labels_ptr != labels_end) {
			int const cluster_idx = *labels_ptr;
			colors_count[cluster_idx]++;
			labels_ptr++;
		}
	}

	// Get max (and second max in case max corresponds to black if we use a mask)
	int max_idx = 0;
	for (int i = 1; i < cluster_count; i++) {
		if (colors_count[i] > colors_count[max_idx])
			max_idx = i;
	}

	int sec_max_idx = max_idx == 0 ? 1 : 0;
	for (int i = 0; i < cluster_count; i++) {
		if (i != max_idx && colors_count[i] > colors_count[sec_max_idx])
			sec_max_idx = i;
	}

	Scalar dominant_color = vecToScalar(centers.row(max_idx));

	if (dominant_color[0] == 0 && dominant_color[1] == 0 && dominant_color[2] == 0) {
		dominant_color = vecToScalar(centers.row(sec_max_idx));
	}
	
	return dominant_color;
}

// Compute bounding rectangle with optional multiplicative factor given contours
Rect getBoundingRect(const Mat& img, const vector<vector<Point>>& hand_contours, double fact = 1.0) {
	// Get bounding rectangle of all contours
	vector<Point> full_contours;
	for (int i = 0; i < hand_contours.size(); i++)
		full_contours.insert(full_contours.end(), hand_contours[i].begin(), hand_contours[i].end());

	if (full_contours.empty())
		return Rect(Point(0, 0), Size(img.cols, img.rows));

	Rect bound_rect = boundingRect(Mat(full_contours));

	// Increase bounding rectangle size with multiplicative fact
	if (fact != 1.0) {
		int half_f_width = int(fact * bound_rect.width / 2.),
			half_f_height = int(fact * bound_rect.height / 2.);

		Point top_left(max(0, bound_rect.x - half_f_width), max(0, bound_rect.y - half_f_height)),
			bottom_right(min(img.cols - 1, bound_rect.x + bound_rect.width + half_f_width), min(img.rows - 1, bound_rect.y + bound_rect.height + half_f_height));

		bound_rect = Rect(top_left, bottom_right);
	}

	return bound_rect;
}

// To generate a series of increasing integers
struct IncGenerator {
	int current_;
	IncGenerator(int start) : current_(start) {}
	int operator() () { return current_++; }
};

// Compute initial colors of image given the input_zone
void initAvgs(const Mat& frame, const Rect& input_zone, vector<Scalar>& hand_avgs, vector<Scalar>& out_hand_avgs) {
	// Add dominant color of input_zone to hand colors
	Mat hand = frame(input_zone);
	//Scalar hand_avg = mean(hand);
	Scalar dominant_color = getDominantColor(hand);

	hand_avgs.clear();
	//hand_avgs.push_back(hand_avg);
	hand_avgs.push_back(dominant_color);
	out_hand_avgs.clear();

	// Consider points of the frame in a pseudo-random order so that it does not consider the top colors first
	vector<int> shuffled_rows(input_zone.height), shuffled_cols(input_zone.width);
	IncGenerator g(0);
	std::generate(shuffled_rows.begin(), shuffled_rows.end(), g);
	std::generate(shuffled_cols.begin(), shuffled_cols.end(), g);
	std::random_shuffle(shuffled_rows.begin(), shuffled_rows.end());
	std::random_shuffle(shuffled_cols.begin(), shuffled_cols.end());

	for (vector<int>::iterator ip = shuffled_rows.begin(); ip != shuffled_rows.end(); ip++) {
		int i = *ip;
		for (vector<int>::iterator jp = shuffled_cols.begin(); jp != shuffled_cols.end(); jp++) {
			int j = *jp;

			Scalar color = frame.at<Vec3b>(i, j);

			// Add hand colors
			if (closestColorNorm(color, hand_avgs) > norm_hand_dist / 2 && closestColorNorm(color, hand_avgs) < norm_hand_dist)
				hand_avgs.push_back(color);
		}
	}

	shuffled_rows = vector<int>(frame.rows);
	shuffled_cols = vector<int>(frame.cols);
	std::generate(shuffled_rows.begin(), shuffled_rows.end(), g);
	std::generate(shuffled_cols.begin(), shuffled_cols.end(), g);
	std::random_shuffle(shuffled_rows.begin(), shuffled_rows.end());
	std::random_shuffle(shuffled_cols.begin(), shuffled_cols.end());

	for (vector<int>::iterator ip = shuffled_rows.begin(); ip != shuffled_rows.end(); ip++) {
		int i = *ip;
		for (vector<int>::iterator jp = shuffled_cols.begin(); jp != shuffled_cols.end(); jp++) {
			int j = *jp;

			Scalar color = frame.at<Vec3b>(i, j);

			// Add background colors
			if (!input_zone.contains(Point(j, i)) && closestColorNorm(color, hand_avgs) > norm_hand_dist && (out_hand_avgs.empty() || closestColorNorm(color, out_hand_avgs) > norm_color_dist))
				out_hand_avgs.push_back(color);
		}
	}
}

// Compute new colors from previous graph cut image
void computeAvgs(const Mat& frame, const Mat1b& final_cuts, vector<Scalar>& hand_avgs, vector<Scalar>& out_hand_avgs) {

	// Skip if we keep hand colors
	if (keep_hand_avgs)
		return;

	out_hand_avgs.clear();

	vector<Scalar> new_hand_avgs;

	/// Consider points of the frame in a pseudo-random order so that it does not consider the top colors first
	vector<int> shuffled_rows(frame.rows), shuffled_cols(frame.cols);
	IncGenerator g(0);
	generate(shuffled_rows.begin(), shuffled_rows.end(), g);
	generate(shuffled_cols.begin(), shuffled_cols.end(), g);
	random_shuffle(shuffled_rows.begin(), shuffled_rows.end());
	random_shuffle(shuffled_cols.begin(), shuffled_cols.end());

	for (vector<int>::iterator ip = shuffled_rows.begin(); ip != shuffled_rows.end(); ip++) {
		int i = *ip;
		for (vector<int>::iterator jp = shuffled_cols.begin(); jp != shuffled_cols.end(); jp++) {
			int j = *jp;

			Scalar color = frame.at<Vec3b>(i, j);

			// Find hand colors from graph cut mask
			if (final_cuts.at<uchar>(i, j) != uchar(0)
				&& closestColorNorm(color, hand_avgs) < norm_hand_dist / 2
				&& (new_hand_avgs.empty() || (closestColorNorm(color, new_hand_avgs) > norm_hand_dist && closestColorNorm(color, new_hand_avgs) < 2 * norm_hand_dist)))
				new_hand_avgs.push_back(color);
		}
	}

	if (!new_hand_avgs.empty())
		hand_avgs = vector<Scalar>(new_hand_avgs);

	for (vector<int>::iterator ip = shuffled_rows.begin(); ip != shuffled_rows.end(); ip++) {
		int i = *ip;
		for (vector<int>::iterator jp = shuffled_cols.begin(); jp != shuffled_cols.end(); jp++) {
			int j = *jp;

			Scalar color = frame.at<Vec3b>(i, j);

			// Find background colors from graph cut mask
			if (final_cuts.at<uchar>(i, j) == uchar(0)
				&& closestColorNorm(color, hand_avgs) > norm_hand_dist
				&& (out_hand_avgs.empty() || closestColorNorm(color, out_hand_avgs) > norm_color_dist))
				out_hand_avgs.push_back(color);
		}
	}
}

// Same in a region of interest
void computeAvgsRoi(const Mat& frame, const Mat1b& final_cuts, const Rect& roi, vector<Scalar>& hand_avgs, vector<Scalar>& out_hand_avgs) {

	// Skip if we keep hand colors
	if (keep_hand_avgs)
		return;

	out_hand_avgs.clear();

	vector<Scalar> new_hand_avgs;

	// Consider points of the frame in a pseudo-random order so that it does not consider the top colors first
	vector<int> shuffled_rows(roi.height), shuffled_cols(roi.width);
	IncGenerator g(0);
	generate(shuffled_rows.begin(), shuffled_rows.end(), g);
	generate(shuffled_cols.begin(), shuffled_cols.end(), g);
	random_shuffle(shuffled_rows.begin(), shuffled_rows.end());
	random_shuffle(shuffled_cols.begin(), shuffled_cols.end());

	for (vector<int>::iterator ip = shuffled_rows.begin(); ip != shuffled_rows.end(); ip++) {
		int i = *ip;
		for (vector<int>::iterator jp = shuffled_cols.begin(); jp != shuffled_cols.end(); jp++) {
			int j = *jp;

			Scalar color = frame.at<Vec3b>(i, j);

			// Find hand colors from graph cut mask
			if (final_cuts.at<uchar>(i, j) != uchar(0)
				&& closestColorNorm(color, hand_avgs) < norm_hand_dist / 2
				&& (new_hand_avgs.empty() || (closestColorNorm(color, new_hand_avgs) > norm_hand_dist && closestColorNorm(color, new_hand_avgs) < 2 * norm_hand_dist)))
				new_hand_avgs.push_back(color);
		}
	}

	if (!new_hand_avgs.empty())
		hand_avgs = vector<Scalar>(new_hand_avgs);

	for (vector<int>::iterator ip = shuffled_rows.begin(); ip != shuffled_rows.end(); ip++) {
		int i = *ip;
		for (vector<int>::iterator jp = shuffled_cols.begin(); jp != shuffled_cols.end(); jp++) {
			int j = *jp;

			Scalar color = frame.at<Vec3b>(i, j);

			// Find background colors out of graph cut mask
			if (final_cuts.at<uchar>(i, j) == uchar(0)
				&& closestColorNorm(color, hand_avgs) > norm_hand_dist
				&& (out_hand_avgs.empty() || closestColorNorm(color, out_hand_avgs) > norm_color_dist))
				out_hand_avgs.push_back(color);
		}
	}
}

// Compute connected components at initialization given the input_zone and the graph cut result
void initConnectedComponents(const Mat& cuts, const Rect& input_zone, Mat1b& final_cuts) {
	// Get connected components labels
	Mat labels(cuts.size(), CV_32S);
	int nLabels = connectedComponents(cuts, labels, 8);

	// Find labels that intersect input_zone
	vector<int> zone_labels(nLabels);
	for (int i = input_zone.y; i < input_zone.y + input_zone.height; i++) {
		for (int j = input_zone.x; j < input_zone.x + input_zone.width; j++) {
			int label = labels.at<int>(i, j);
			zone_labels[label] = label == 0 ? 0 : 1;
		}
	}

	// Compute new final_cuts
	final_cuts.copyTo(prev_cuts);
	final_cuts = Mat1b::zeros(cuts.size());
	for (int i = 0; i < final_cuts.rows; i++) {
		for (int j = 0; j < final_cuts.cols; j++) {
			if (zone_labels[labels.at<int>(i, j)] == 1)
				final_cuts.at<uchar>(i, j) = uchar(255);
		}
	}
}

// Same after initialization
void getConnectedComponents(const Mat& cuts, Mat1b& final_cuts) {
	// Get connected components labels
	Mat labels(cuts.size(), CV_32S);
	int nLabels = connectedComponents(cuts, labels, 8);

	// Find labels that intersect final_cuts
	vector<int> zone_labels(nLabels);
	for (int i = 0; i < labels.rows; i++) {
		for (int j = 0; j < labels.cols; j++) {
			if (final_cuts.at<uchar>(i, j) == uchar(255)) {
				int label = labels.at<int>(i, j);
				zone_labels[label] = label == 0 ? 0 : 1;
			}
		}
	}

	// Compute new final_cuts
	final_cuts.copyTo(prev_cuts);
	final_cuts = Mat1b::zeros(cuts.size());
	for (int i = 0; i < final_cuts.rows; i++) {
		for (int j = 0; j < final_cuts.cols; j++) {
			if (zone_labels[labels.at<int>(i, j)] == 1)
				final_cuts.at<uchar>(i, j) = uchar(255);
		}
	}
}

// Return contours of final graph cuts and make some checks
void getContours(Mat1b& final_cuts, vector<vector<Point>>& hand_contours) {
	// Find contours and compute their areas
	vector<vector<Point>> prev_contours(hand_contours);
	findContours(final_cuts, hand_contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
	double prev_area_sum = area_sum;
	area_sum = 0;
	vector<double> contour_areas;
	for (int k = 0; k < hand_contours.size(); k++) {
		contour_areas.push_back(contourArea(hand_contours[k]));
		area_sum += contour_areas[k];
	}

	// If area is too important then the graph cut must include background and is refused, keep important data
	if (area_sum > final_cuts.rows * final_cuts.cols * max_area_fact) {
		lost = true;
		hand_contours = vector<vector<Point>>(prev_contours);
		area_sum = prev_area_sum;
		final_cuts = Mat1b::zeros(prev_cuts.size());
		prev_cuts.copyTo(final_cuts);
		return;
	}

	// Keep hand colors if they give a better result in a same region of the image
	double area_ratio = area_sum / prev_area_sum;

	Rect prev_br = getBoundingRect(final_cuts, prev_contours, br_keep_lim), br = getBoundingRect(final_cuts, hand_contours);
	bool in_prev_br = (prev_br.width * prev_br.height >= br.width * br.height);
	if (in_prev_br && ((keep_hand_avgs && area_ratio > 0.9) || area_ratio > 1.0))
		keep_hand_avgs = true;
	else
		keep_hand_avgs = false;

	if ((initiation || in_prev_br) && lost)
		lost = false;
}


/// MOVEMENT

Point tlBlock(Point p) {
	return Point(floor(p.x / grid_cols), floor(p.y / grid_rows));
}

Point brBlock(Point p) {
	return Point(ceil(p.x / grid_cols), ceil(p.y / grid_rows));
}

Point blockToPoint(Point p) {
	return Point((p.x + 1) * grid_cols - 1, (p.y + 1) * grid_rows - 1);
}

void inBox(Point& p) {
	if (p.x < 0)
		p.x = 0;
	else if (p.x > width - 1)
		p.x = width - 1;
	if (p.y < 0)
		p.y = 0;
	else if (p.y > height - 1)
		p.y = height - 1;
}

void updateBackground(Mat& memory_frame, vector<vector<int>>& memory_relevance, int i, int j, Vec3b new_color) {
	/// if we are unsure the block is par of the background yet 
	if (memory_relevance[i][j] < 20) {
		memory_frame.at<Vec3b>(i, j) = memory_frame.at<Vec3b>(i, j) * (1 - bg_update_velocity) + new_color * (bg_update_velocity);
		if (norm(memory_frame.at<Vec3b>(i, j) - new_color) < background_thresh) {
			memory_relevance[i][j]++;
		}
		else {
			memory_relevance[i][j]--;
		}
	}
	/// We are pretty sure it is a background but we still change the color a little
	else {
		memory_frame.at<Vec3b>(i, j) = memory_frame.at<Vec3b>(i, j) * (1 - bg_update_velocity / 2) + new_color * (bg_update_velocity / 2);
		if (norm(memory_frame.at<Vec3b>(i, j) - new_color) < background_thresh)
			if (memory_relevance[i][j] < 100 * bg_update_velocity)
				memory_relevance[i][j]++;
			else {
				memory_relevance[i][j] -= 5;
			}
	}
}

int direction(const Point& p) {
	if (abs(p.x) > abs(p.y)) {
		if (p.x > 0)
			return 0;
		else
			return 1;
	}
	else {
		if (p.y > 0)
			return 2;
		else
			return 3;
	}
}

string directionToString(int a) {
	if (a == 0)
		return "right";
	else if (a == 1)
		return "left";
	else if (a == 2)
		return "down";
	else
		return "up";
}

Rect handZone(Point p, const vector<vector<int>>& diffs) {
	/// Computes a rectangle around the hand with a point p as indicator of where the hand approximately is 
	Point vertex(p.x - grid_cols * grid_count / 4, p.y - grid_cols * grid_count / 3), oppositevertex(p.x + grid_cols * grid_count / 4, p.y + grid_cols * grid_count / 3);
	inBox(vertex); inBox(oppositevertex);

	/// Draw a big rectangle around the hand
	Rect zone(tlBlock(vertex), brBlock(oppositevertex));
	float sumL = 0, sumR = 0, sumT = 0, sumB = 0;
	bool finished = false;
	Point tl = zone.tl(), br = zone.br();


	while (!finished && tl.x < br.x - 1 && tl.y < br.y - 1) {
		finished = true;
		/// Compute the value of the exterior rows and cols 
		for (int k = tl.x; k < zone.width; k++) {
			if (diffs[k][tl.y] > 0)
				sumT++;
			if (diffs[k][br.y] > 0)
				sumB++;
		}
		for (int k = tl.y; k < zone.height; k++) {
			if (diffs[br.x][k] > 0)
				sumR++;
			if (diffs[tl.x][k] > 0)
				sumL++;
		}

		/// If their difference to the background is very low get rid of them else the hand is probably here 
		if (sumL < zone.width / hand_zone_thresh && (float)zone.width / (float)zone.height > 0.5) {
			tl = tl + Point(1, 0);
			inBox(tl);
			zone = Rect(tl, br);
			finished = false;
		}
		if (sumR < zone.width / hand_zone_thresh && (float)zone.width / (float)zone.height > 0.5) {
			br = br + Point(-1, 0);
			inBox(br);
			zone = Rect(tl, br);
			finished = false;
		}
		if (sumT < zone.height / hand_zone_thresh && zone.width / zone.height < 1) {
			tl = tl + Point(0, 1);
			inBox(tl);
			zone = Rect(tl, br);
			finished = false;
		}
		if (sumB < zone.height / hand_zone_thresh && zone.width / zone.height < 1) {
			br = br + Point(0, -1);
			inBox(br);
			zone = Rect(tl, br);
			finished = false;
		}

	}

	return Rect(blockToPoint(tl), blockToPoint(br));
}
/* Computes relevant point, was used before starting to compute the movement's history*/

tuple<Point, Point, vector<vector<int>>> relevantPoint(const Mat& frame, Mat& move_frame, Mat& velocity, Mat& memory_frame, vector<vector<int>>& memory_relevance) {
	// Compute the barycenter of a frame given the difference with the previous frame
	//Point bar(0, 0);

	int size = 0;
	Point topacc(-1, -1), bardiff(0, 0);
	vector<vector<int>> diffs(grid_count, vector<int>(grid_count, 0));
	/// Compute difference between frames
	for (int j = 0; j < grid_count; j++) {
		for (int i = 0; i < grid_count; i++) {
			///Define a rectangle (block) and calculate de change between now and last frame
			Point vertex(i*grid_cols, j*grid_rows), oppositevertex((i + 1)*grid_cols, (j + 1)*grid_rows);
			Rect r(vertex, oppositevertex);
			Scalar mean_color_s = mean(frame(r));
			Vec3b mean_color(mean_color_s[0], mean_color_s[1], mean_color_s[2]);
			/// Substract the background to the new color
			Vec3b diff = mean_color - memory_frame.at<Vec3b>(i, j);
			Vec3b acc = diff - velocity.at<Vec3b>(i, j);
			updateBackground(memory_frame, memory_relevance, i, j, mean_color);
			rectangle(move_frame, r, diff, CV_FILLED);
			velocity.at<Vec3b>(i, j) = diff;
			if (norm(diff) > color_thresh) {
				bardiff.x += (i + 0.5) * grid_cols;
				bardiff.y += (j + 0.5) * grid_rows;
				size++;
			}

			if (norm(diff) > color_thresh / 2) {
				diffs[i][j] = 1;
			}
			if (topacc.x == -1 && norm(acc) > color_thresh)
				topacc = vertex;
		}
	}
	if (size < move_size_thresh)
		bardiff = Point(-1, -1);
	else
		bardiff = bardiff / size;

	return tuple<Point, Point, vector<vector<int>>>(topacc, bardiff, diffs);
}

tuple<bool, Point, vector<vector<int>>> initMove(const Mat& frame, Mat& move_frame, Mat& velocity, Mat& memory_frame, vector<vector<int>>& memory_relevance, Mat& move_history, int moving_time) {
	/* Return the barycenter of a frame given the difference with the previous frame and a diff vector containing the differences with the background */
	/* Updates the velocity, background and the move_history matrices */

	bool moved = false;
	Point bar = Point(0, 0);
	int size = 0;
	vector<vector<bool>> changed(grid_count, vector<bool>(grid_count, false));
	vector<vector<int>> diffs(grid_count, vector<int>(grid_count, 0));
	/// Compute difference between frames
	for (int j = 0; j < grid_count; j++) {
		for (int i = 0; i < grid_count; i++) {
			///Define a rectangle (block) and calculate de change between now and last frame
			Point vertex(i*grid_cols, j*grid_rows), oppositevertex((i + 1)*grid_cols, (j + 1)*grid_rows);
			Rect r(vertex, oppositevertex);
			Scalar mean_color_s = mean(frame(r));
			Vec3b mean_color(mean_color_s[0], mean_color_s[1], mean_color_s[2]);
			/// Substract the background to the new color to see what moved
			Vec3b diff = mean_color - memory_frame.at<Vec3b>(i, j);
			/// Compute the acceleration to see what is moveing
			Vec3b acc = diff - velocity.at<Vec3b>(i, j);
			updateBackground(memory_frame, memory_relevance, i, j, mean_color);
			velocity.at<Vec3b>(i, j) = diff;

			/// Compute the barycenter of the points that moved for the hand zone 
			if (norm(diff) > color_thresh / 2)
				diffs[i][j] = 1;
			if (norm(diff) > color_thresh) {
				bar.x += (i + 0.5) * grid_cols;
				bar.y += (j + 0.5) * grid_rows;
				size++;
			}
			///If there was a change
			if (norm(acc) > color_thresh / 2) {
				moved = true;
				if (move_history.at<int>(i, j) == 0) {
					int neigh_hist = 0;
					/// Check if the neighbors have moved before or at the same time and set the value accordingly
					if (i > 0 && j > 0 && i < grid_count - 1 && j < grid_count - 1) {
						neigh_hist = move_history.at<int>(i - 1, j);
						if (move_history.at<int>(i + 1, j) > neigh_hist && !changed[i + 1][j])
							neigh_hist = move_history.at<int>(i + 1, j);
						if (move_history.at<int>(i, j - 1) > neigh_hist && !changed[i][j - 1])
							neigh_hist = move_history.at<int>(i, j - 1);
						if (move_history.at<int>(i, j + 1) > neigh_hist && !changed[i][j + 1])
							neigh_hist = move_history.at<int>(i, j + 1);
						if (move_history.at<int>(i + 1, j + 1) > neigh_hist && !changed[i + 1][j + 1])
							neigh_hist = move_history.at<int>(i + 1, j + 1);
						if (move_history.at<int>(i - 1, j - 1) > neigh_hist && !changed[i - 1][j - 1])
							neigh_hist = move_history.at<int>(i - 1, j - 1);
						if (move_history.at<int>(i - 1, j + 1) > neigh_hist && !changed[i - 1][j + 1])
							neigh_hist = move_history.at<int>(i - 1, j + 1);
						if (move_history.at<int>(i + 1, j - 1) > neigh_hist && !changed[i + 1][j - 1])
							neigh_hist = move_history.at<int>(i + 1, j - 1);

						//neigh_hist = max({ move_history.at<int>(i + 1, j) - changed[i + 1][j] + 1, move_history.at<int>(i, j + 1) - changed[i][j + 1] + 1, 
						//	move_history.at<int>(i - 1, j) - changed[i - 1][j] + 1, move_history.at<int>(i + 1, j) - changed[i + 1][j] + 1 });
					}
					else
						neigh_hist = 0;
					changed[i][j] = 1;
					move_history.at<int>(i, j) = min(neigh_hist + 1, moving_time + 1);
				}
			}
			//rectangle(move_frame, r, acc, CV_FILLED);
			//rectangle(move_frame, r, diff, CV_FILLED);
			rectangle(move_frame, r, Scalar(15, 15, 15) * move_history.at<int>(i, j), CV_FILLED);
		}
	}
	if (size < move_size_thresh)
		bar = Point(-1, -1);
	else
		bar = bar / size;
	return tuple<bool, Point, vector<vector<int>>>(moved, bar, diffs);
}

Point barOfValue(const Mat& move_history, int moving_time) {
	/// Returns the barycenter of all points with value moving_time 
	Point bar(0, 0);
	double size = 0;
	for (int i = 0; i < grid_count; i++) {
		for (int j = 0; j < grid_count; j++) {
			if (move_history.at<int>(i, j) == moving_time) {
				bar.x += (i + 0.5) * grid_cols;
				bar.y += (j + 0.5) * grid_rows;
				size++;
			}
		}
	}
	if (size > 0)
		bar = bar / size;
	else
		bar = Point(-1, -1);
	return bar;
}



bool relevantShake(const Mat& move_history, int move_time) {
	double min_move, max_move;
	minMaxLoc(move_history, &min_move, &max_move);
	if (max_move * 1.5 < move_time && max_move > shake_size_thresh && move_time > shake_duration_thresh)
		return true;
	else
		return false;
}

bool relevantMomentum(double m, int dir) {
	if (dir == 3)
		return (m > momentum_thresh / 2);
	else
		return (m > momentum_thresh);
}

/* Old movement function used whit a characteristic point  */
bool movement(vector<Point>& point_list, const Mat& frame, Mat& input_frame, time_t move_start) {
	int moved = false;
	int n = point_list.size();
	if (n > 0) {
		for (int i = 0; i < n; i++) {
			circle(input_frame, point_list[i], 5, Scalar(i * 255 / n, i * 255 / n, i * 255 / n), CV_FILLED);
		}
	}
	else
		return 0;
	/// If the movement is relevent, we find the global direction, and do a histogram
	if (n > move_duration_thresh) {
		/// values 
		Point move = point_list[n - 1] - point_list[0];
		n = point_list.size();
		double momentum = norm(move) / (double(clock() - move_start) * frame.rows / CLOCKS_PER_SEC);

		/// Find the main direction and its relation to the second most represented direction 

		if (relevantMomentum(momentum, direction(move))) {
			arrowedLine(input_frame, point_list[0], point_list[n - 1], Scalar(0, 255, 0), 5);
			putText(input_frame, "Direction : " + directionToString(direction(move)), Point(grid_count, grid_count), 0, 1, Scalar(100, 100, 100), 3);
			cout << " Detected Direction : " << directionToString(direction(move)) << endl;
			moved = true;
		}
	}
	return moved;
}


int movement(const Mat& move_history, Mat& input_frame, int moving_time, Point start_bar) {
	/// Takes the movement history, the starting point and the movement's duration and return if the movement is relevant 
	double max_move, min_move;
	minMaxLoc(move_history, &min_move, &max_move);

	Point bar_max = barOfValue(move_history, max_move);
	/// If the movement is relevent, we find the global direction, and do a histogram

	if (moving_time > move_duration_thresh) {
		/// values 
		Point move = bar_max - start_bar;
		double momentum = norm(move) * 100 / (moving_time * width);
		/// Find the main direction and its relation to the second most represented direction 

		if (relevantMomentum(momentum, direction(move))) {
			arrowedLine(input_frame, start_bar, bar_max, Scalar(0, 255, 0), 5);
			putText(input_frame, "Direction : " + directionToString(direction(move)), Point(grid_count, grid_count), 0, 1, Scalar(100, 100, 100), 3);
			cout << " Detected Direction : " << directionToString(direction(move)) << endl;
			return 1;
		}
		else if (max_move * 1.4 < moving_time && moving_time > shake_duration_thresh) {
			putText(input_frame, "Shake detected !", Point(grid_count, grid_count), 0, 1, Scalar(100, 100, 100), 3);
			return 2;
		}
	}
	return 0;
}

/// LEARNING

// Deskew image before computing descriptors
Mat deskew(const Mat& I) {
	Moments M;
	if (I.channels() != 1) {
		Mat gray;
		cvtColor(I, gray, CV_BGR2GRAY);
		M = moments(gray, true);
	}
	else
		M = moments(I, true);
	if (abs(M.mu02) < 0.01)
		return I.clone();
	double skew = M.mu11 / M.mu02;
	Matx23d mtx(1, skew, -0.5*training_img_size.width*skew, 0, 1, 0);
	Mat deskewed;
	warpAffine(I, deskewed, mtx, training_img_size, CV_WARP_INVERSE_MAP | INTER_LINEAR);
	return deskewed;
}

// Descriptor object
HOGDescriptor hog(
	training_img_size, //winSize
	training_img_size / 2, //blocksize
	training_img_size / 4, //blockStride,
	training_img_size / 2, //cellSize,
	16, //nbins,
	1, //derivAper,
	-1, //winSigma,
	0, //histogramNormType,
	0.2, //L2HysThresh,
	1,//gammal correction,
	64,//nlevels=64
	1);//Use signed gradients

// Extract region of interest from graph cut image given contours to compute prediction
Mat extractRoi(const Mat& img, const vector<vector<Point>>& hand_contours) {
	Rect bound_rect = getBoundingRect(img, hand_contours);

	Mat roi = img(bound_rect);
	resize(roi, roi, training_img_size);

	return roi;
}

// Load path for training images
String loadPattern(String& folder) {
	return String("../training_images/" + folder + "/*.jpg");
}

// Convert image to descriptors
vector<float> imgToHog(Mat& img) {
	vector<float> descriptors;
	hog.compute(deskew(img), descriptors);
	return descriptors;
}

// Load training descriptors
void loadTrainHogs(vector<vector<float>>& train_hogs, vector<int>& train_labels) {
	vector<String> fn;

	for (int i = 0; i < training_sets_count; i++) {
		glob(loadPattern(training_sets[i]), fn, true);
		for (size_t k = 0; k < fn.size(); k++) {
			Mat img = imread(fn[k]);
			if (img.empty())
				continue;
			Mat mirror_img(img.size(), img.type());
			flip(img, mirror_img, 0);
			train_hogs.push_back(imgToHog(img));
			train_hogs.push_back(imgToHog(mirror_img));
			train_labels.push_back(i);
			train_labels.push_back(i);
		}
	}
}

Mat asMat(const vector<vector<float>>& hogs) {
	int descriptor_size = hogs[0].size();
	Mat mat(hogs.size(), descriptor_size, CV_32F);

	for (int i = 0; i < hogs.size(); i++) {
		for (int j = 0; j < descriptor_size; j++)
			mat.at<float>(i, j) = hogs[i][j];
	}

	return mat;
}

// Initialize SVM
Ptr<SVM> svmInit(float C, float gamma) {
	Ptr<SVM> svm = SVM::create();
	svm->setGamma(gamma);
	svm->setC(C);
	svm->setKernel(SVM::RBF);
	svm->setType(SVM::C_SVC);

	return svm;
}

// Train SVM from train matrix of descriptors and labels
void svmTrain(Ptr<SVM> svm, Mat& train_mat, vector<int>& train_labels) {
	Ptr<TrainData> td = TrainData::create(train_mat, ROW_SAMPLE, Mat(train_labels));
	svm->train(td);
	svm->save(classifier_path);
}

// Load svm from file
void svmLoad(Ptr<SVM> svm, String& path) {
	svm = Algorithm::load<SVM>(path);
}

// Predict on image
int svmPredict(Ptr<SVM> svm, Mat& img) {
	vector<float> hog = imgToHog(img);
	/*Mat hog_mat(1, int(hog.size()), CV_32F);
	for (int i = 0; i < int(hog.size()); i++)
	hog_mat.at<float>(0, i) = hog[i];*/
	float pose = svm->predict(hog);
	return int(pose);
}


int main()
{
	/// Learning initialization
	float C = 12.5, gamma = 0.5;
	Ptr<SVM> svm = svmInit(C, gamma);
	int frame_number = 0;

	if (svm_enabled) {
		//svmLoad(svm, classifier_path);

		vector<vector<float>> train_hogs;
		vector<int> train_labels;
		loadTrainHogs(train_hogs, train_labels);
		Mat train_mat = asMat(train_hogs);
		svmTrain(svm, train_mat, train_labels);
	}

	/// Create windows
	//namedWindow(input_window, CV_WINDOW_AUTOSIZE);
	namedWindow(input_result_window, CV_WINDOW_AUTOSIZE);
	namedWindow(control_window, CV_WINDOW_AUTOSIZE);
	namedWindow(gray_window, CV_WINDOW_AUTOSIZE);

	/// Capture from webcam
	VideoCapture cap(cap_idx);
	if (!cap.isOpened()) {
		printf("Error loading video capture");
		return -1;
	}

	Mat frame;
	cap.read(frame);

	/// Initialize cuts with frame size
	resize(frame, frame, Size(width, height));
	Mat cuts = Mat::zeros(frame.size(), CV_8UC3);
	Mat1b final_cuts = Mat1b::zeros(cuts.size());
	int prev_pose = -1;

	/// Create rectangle input zone for hand
	int h_border = frame.cols / 3,
		v_border = frame.rows / 10;
	Point i_vertex(h_border, v_border),
		i_opposite_vertex(frame.cols - h_border, frame.rows - v_border);
	Rect input_zone(i_vertex, i_opposite_vertex);


	/// Movement values
	time_t move_start = clock();
	bool moving = false;
	int steady_time = 0;
	int moving_time = 0;
	Mat velocity(grid_count, grid_count, CV_8UC3);
	Mat memory_frame = Mat::zeros(grid_count, grid_count, CV_8UC3);
	Mat move_history = Mat::zeros(grid_count, grid_count, CV_32SC1);
	Point start_bar(0, 0);

	vector<vector<int>> memory_relevance(grid_count, vector<int>(grid_count, 0));
	/// For relevant point use
	vector<Point> point_list;


	int key = waitKey(3);

	/// While space bar isn't pressed
	while (key != 32) {
		/// Wait for background to stabilize
		if (frame_number < 20)
			frame_number++;
		bool frame_ready = (frame_number > 19);
		time_t start = clock();

		/// Exit if space bar is pressed
		key = waitKey(3);

		/// Enter/Exit validated mode (backspace)
		if (key == 8)
			cut_validated = !cut_validated;
		/// Press 0 to restart initiation with hand in front of camera
		else if (key == 48) {
			cout << "0 pressed : reinitialization. Press backspace to validate" << endl;
			Rect input_zone(i_vertex, i_opposite_vertex);
			initiation = true;
			initialized = true;
			lost = false;
		}
		/// Press enter to initialize the cut manually 
		else if (key == 13) {
			cout << "Enter pressed, hand color reinitialized" << endl;
			initialized = false;
			lost = true;
			hand_avgs.clear();
		}

		/// Grab new video frame from camera
		cap.read(frame);
		resize(frame, frame, Size(width, height));

		Mat input_frame, gray_frame, ycc_frame, move_frame;

		cvtColor(frame, gray_frame, CV_BGR2GRAY);
		toYCC(frame, ycc_frame);
		frame.copyTo(input_frame);
		gray_frame.copyTo(move_frame);
		///Data for one frame
		bool moved = false;

		/* --- MOVEMENT --- */

		/// Compute difference between frames
		Point bar; bool is_moving;
		vector<vector<int>> diffs;


		///if using the relevant point method
		//Point topacc, bardiff;
		//tie(topacc, bardiff, diffs) = relevantPoint(ycc_frame, move_frame, velocity, memory_frame, memory_relevance);


		tie(is_moving, bar, diffs) = initMove(ycc_frame, move_frame, velocity, memory_frame, memory_relevance, move_history, moving_time);
		/// Detecting barycenters means finding movement, if the point wasn't moving, this is the first moving frame
		if (bar != Point(-1, -1))
			circle(input_frame, bar, 3, Scalar(255, 255, 0), 3);

		if (frame_ready) {
			/// If it wasn't moving
			if (!moving) {
				moving = is_moving;
				/// And now it moves
				if (moving) {
					move_start = clock();
					steady_time = 0;
					moving_time = 1;
					start_bar = bar;
				}
				else
					steady_time++;

				///Find Hand Zone
				/// If nothing is moving since a few frame but we still see the hand draw a hand zone 
				if (!moving && bar.x != -1 && steady_time == 3 && lost) {
					input_zone = handZone(bar, diffs);
					rectangle(input_frame, input_zone, Scalar(0, 255, 0), 3);
					imshow(input_result_window, input_frame);
					waitKey(500);
					if (!initialized) {
						initiation = true;
						initialized = true;
					}
					cut_validated = true;
					if (initialized)
						cout << "Hand zone detected" << endl;
					else
						cout << "Hand zone detected with new hand colors" << endl;
					lost = false;
					cout << "Hand position recognition started" << endl;
					steady_time = 0;

				}
			}
			/// If it was moving 
			else {
				if (start_bar == Point(0, 0) || start_bar == Point(-1, -1))
					start_bar = bar;
				moving = is_moving && moving_time < 15;

				/// If we are still moving we wait for the move to end 
				if (moving) {
					moving_time++;
				}
				/// If it is an end of move, we study the move
				else {
					int moved = 0;
					if (start_bar != Point(0, 0))
						moved = movement(move_history, input_frame, moving_time, start_bar);

					if (!lost && moved == 1) {
						lost = true;
						cout << "Hand position recognition ended because you moved" << endl;
					}
					if (moved == 2) {
						lost = true;
						cout << "Shaking : Hand lost" << endl;
						imshow(input_result_window, input_frame);
						imshow(gray_window, move_frame);

					}
					move_history = Mat::zeros(grid_count, grid_count, CV_32SC1);
					moving_time = 0;
					steady_time = 1;
					start_bar = Point(0, 0);
				}
			}


		}

		/* --- POSE --- */


		if (lost) {
			initiation = false;
			cut_validated = false;
		}

		if (initiation)
			rectangle(input_frame, input_zone, Scalar(0, 0, 255), 3);

		/// If validated mode or Enter is pressed, process graph cuts
		if (cut_validated || initiation) {
			/// If Enter has been pressed, calculate color averages in and out of the zone

			if (initiation)
				initAvgs(ycc_frame, input_zone, hand_avgs, out_hand_avgs);

			/// Make graph cut
			Mat grad, orient;
			derivate(gray_frame, grad, orient);
			if (initiation)
				cuts = gCuts(ycc_frame, grad, hand_avgs, out_hand_avgs);
			else {
				Rect roi = getBoundingRect(ycc_frame, hand_contours, br_fact);
				cuts = gCutsRoi(ycc_frame, grad, roi, hand_avgs, out_hand_avgs);
			}

			/// Remove contours in cuts to separate components
			//removeContours(gray_frame, cuts);
			//imshow(control_window, cuts);

			/// Get connected components from the zone (from input zone to previous hand convex hull)
			if (initiation)
				initConnectedComponents(cuts, input_zone, final_cuts);
			else
				getConnectedComponents(cuts, final_cuts);

			getContours(final_cuts, hand_contours);
			if (lost) {
				imshow(control_window, final_cuts);
				continue;
			}

			//hullNDefects(cuts_to_show, hand_contours);
			imshow(control_window, final_cuts);

			/// Make predictions on pred_length consecutive frames
			if (svm_enabled) {
				int pose = svmPredict(svm, extractRoi(final_cuts, hand_contours));
				if (predictions.size() != pred_length)
					predictions.push_back(pose);
				else {
					int max = 0, dominant_pose = predictions[0];
					for (int i = 0; i < predictions.size(); i++) {
						int co = (int)count(predictions.begin(), predictions.end(), predictions[i]);
						if (co > max) {
							max = co;
							dominant_pose = predictions[i];
						}
					}

					if (prev_pose != -1 && prev_pose != dominant_pose)
						cout << "Your hand position is " << training_sets[dominant_pose] << endl;
					prev_pose = pose;

					predictions.clear();
				}
			}

			/// Compute new colors
			//computeAvgs(ycc_frame, final_cuts, hand_avgs, out_hand_avgs);
			Rect roi = getBoundingRect(ycc_frame, hand_contours, br_fact);
			computeAvgsRoi(ycc_frame, final_cuts, roi, hand_avgs, out_hand_avgs);

			initiation = false;
		}

		imshow(input_result_window, input_frame);
		imshow(gray_window, move_frame);
		//imshow("YCC", ycc_frame);
	}

	cap.release();
	return 0;
}







/// USELESS CODE

// Remove contours of original image on graph cut image
//void removeContours(const Mat& frame, const Mat& cuts) {
//	Mat1b edges;
//	vector<vector<Point>> init_contours;
//	Canny(frame, edges, lowThreshold, lowThreshold*threshRatio, kernel_size);
//	findContours(edges, init_contours, noArray(), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
//	for (int i = 0; i < init_contours.size(); i++)
//		drawContours(cuts, init_contours, i, uchar(0), 3, 8, noArray(), 0, Point());
//}

// Compute hull and convexity defects of graph cut image
//void hullNDefects(Mat& cuts_to_show, vector<vector<Point>> hand_contours) {
//	/// Convex Hull
//	vector<Point> full_contours;
//	vector<vector<Point>> hull(1);
//	for (int i = 0; i < hand_contours.size(); i++)
//		full_contours.insert(full_contours.end(), hand_contours[i].begin(), hand_contours[i].end());
//	convexHull(Mat(full_contours), hull[0]);
//	drawContours(cuts_to_show, hull, 0, Scalar(0, 0, 255), 2, 8, noArray(), 0, Point());
//
//	/// Convexity defects
//	vector<int> hull_indices;
//	convexHull(full_contours, hull_indices, true);
//	vector<Vec4i> defects;
//	convexityDefects(full_contours, hull_indices, defects);
//	for (Vec4i v : defects)
//		arrowedLine(cuts_to_show, full_contours[v[0]], full_contours[v[2]], Scalar(0, 255, 0));
//}

//bool goodDirection(int dir, Point move) {
//	if (dir == 0)
//		return (move.x / abs(move.y + 0.01) > -angular_thresh);
//	else if (dir == 1)
//		return (move.x / abs(move.y + 0.01) < angular_thresh);
//	else if (dir == 2)
//		return (move.y / abs(move.y + 0.01) > -angular_thresh);
//	else
//		return (move.y / abs(move.y + 0.01) < angular_thresh);
//}

//bool relevantDirection(const vector<Point>& point_list, Point move) {
//	int n = point_list.size();
//	int dir = direction(move);
//	int directions_hist[2] = { 0,0 };
//	for (int i = 0; i < n - 1; i++) {
//		if (goodDirection(dir, point_list[i + 1] - point_list[i]))
//			directions_hist[0]++;
//		else
//			directions_hist[1]++;
//	}
//	int direction_dominance = (directions_hist[0] - directions_hist[1]) * 100 / n;
//	//cout << " Direction dominance : " << direction_dominance << endl;
//	//return ((directions_hist[0] - directions_hist[1]) * 100 / n > direction_dominance_thresh);
//	return true;
//}

// Relevant point use
//int main()
//{
//	/// Learning initialization
//	float C = 12.5, gamma = 0.5;
//	Ptr<SVM> svm = svmInit(C, gamma);
//	int frame_number = 0;
//
//	if (svm_enabled) {
//		//svmLoad(svm, classifier_path);
//
//		vector<vector<float>> train_hogs;
//		vector<int> train_labels;
//		loadTrainHogs(train_hogs, train_labels);
//		Mat train_mat = asMat(train_hogs);
//		svmTrain(svm, train_mat, train_labels);
//	}
//
//	/// Create windows
//	//namedWindow(input_window, CV_WINDOW_AUTOSIZE);
//	namedWindow(input_result_window, CV_WINDOW_AUTOSIZE);
//	namedWindow(control_window, CV_WINDOW_AUTOSIZE);
//	namedWindow(gray_window, CV_WINDOW_AUTOSIZE);
//
//	/// Capture from webcam
//	VideoCapture cap(cap_idx);
//	if (!cap.isOpened()) {
//		printf("Error loading video capture");
//		return -1;
//	}
//
//	Mat frame;
//	cap.read(frame);
//
//	/// Initialize cuts with frame size
//	resize(frame, frame, Size(width, height));
//	Mat cuts = Mat::zeros(frame.size(), CV_8UC3);
//	Mat1b final_cuts = Mat1b::zeros(cuts.size());
//	int prev_pose = -1;
//
//	/// Create rectangle input zone for hand
//	int h_border = frame.cols / 3,
//		v_border = frame.rows / 10;
//	Point i_vertex(h_border, v_border),
//		i_opposite_vertex(frame.cols - h_border, frame.rows - v_border);
//	Rect input_zone(i_vertex, i_opposite_vertex);
//
//
//	/// Movement values
//	time_t move_start = clock();
//	bool moving = false;
//	int steady_time = 0;
//	int moving_time = 0;
//	Mat velocity(grid_count, grid_count, CV_8UC3);
//	Mat memory_frame = Mat::zeros(grid_count, grid_count, CV_8UC3);
//	Mat move_history = Mat::zeros(grid_count, grid_count, CV_32SC1);
//	Point start_bar(0, 0);
//
//	vector<vector<int>> memory_relevance(grid_count, vector<int>(grid_count, 0));
//	/// For relevant point use
//	vector<Point> point_list;
//
//
//	int key = waitKey(3);
//
//	/// While space bar isn't pressed
//	while (key != 32) {
//		/// Wait for background to stabilize
//		if (frame_number < 20)
//			frame_number++;
//		bool frame_ready = (frame_number > 19);
//		time_t start = clock();
//
//		/// Exit if space bar is pressed
//		key = waitKey(3);
//
//		/// Enter/Exit validated mode (backspace)
//		if (key == 8)
//			cut_validated = !cut_validated;
//		/// Press 0 to restart initiation with hand in front of camera
//		else if (key == 48) {
//			cout << "0 pressed : reinitialization. Press backspace to validate" << endl;
//			Rect input_zone(i_vertex, i_opposite_vertex);
//			initiation = true;
//			initialized = true;
//			lost = false;
//		}
//		/// Press enter to initialize the cut manually 
//		else if (key == 13) {
//			cout << "Enter pressed, hand color reinitialized" << endl;
//			initialized = false;
//			lost = true;
//			hand_avgs.clear();
//		}
//
//		/// Grab new video frame from camera
//		cap.read(frame);
//		resize(frame, frame, Size(width, height));
//
//		Mat input_frame, gray_frame, ycc_frame, move_frame;
//
//		cvtColor(frame, gray_frame, CV_BGR2GRAY);
//		toYCC(frame, ycc_frame);
//		frame.copyTo(input_frame);
//		gray_frame.copyTo(move_frame);
//		///Data for one frame
//		bool moved = false;
//
//		/* --- MOVEMENT --- */
//
//		/// Compute difference between frames
//		Point bar; bool is_moving;
//		vector<vector<int>> diffs;
//
//
//		///if using the relevant point method
//		//Point topacc, bardiff;
//		//tie(topacc, bardiff, diffs) = relevantPoint(ycc_frame, move_frame, velocity, memory_frame, memory_relevance);
//
//
//		tie(is_moving, bar, diffs) = initMove(ycc_frame, move_frame, velocity, memory_frame, memory_relevance, move_history, moving_time);
//		/// Detecting barycenters means finding movement, if the point wasn't moving, this is the first moving frame
//		if (bar != Point(-1, -1))
//			circle(input_frame, bar, 3, Scalar(255, 255, 0), 3);
//
//
//		/// for relevant Point use
//
//		if (frame_ready) {
//			if (!moving) {
//				if (topacc.x != -1)
//					move_start = clock();
//				moving = (topacc.x != -1);
//				if (!moving) {
//					steady_time++;
//					//circle(input_frame, bardiff, 5, Scalar(255, 0, 255), CV_FILLED);
//				}
//
//
//				///Find Hand Zone
//				/// If nothing is moving since a few frame but we still see the hand draw a hand zone
//				if (!moving && bardiff.x != -1 && steady_time > 1 && lost) {
//					input_zone = handZone(bardiff, diffs);
//					rectangle(move_frame, input_zone, Scalar(255, 0, 0), 3);
//					imshow(input_result_window, input_frame);
//					steady_time = 0;
//					Scalar dominant_color = getDominantColor(ycc_frame(input_zone));
//					if (closestColorNorm(dominant_color, hand_avgs) > hand_color_thresh) {
//						cout << "The zone detected does not seem to contain a hand" << endl;
//					}
//					else {
//						if (!initialized) {
//							initiation = true;
//							initialized = true;
//						}
//						cut_validated = true;
//						if (initialized)
//							cout << "Hand zone detected" << endl;
//						else
//							cout << "Hand zone detected with new hand colors" << endl;
//						lost = false;
//						cout << "Hand position recognition started" << endl;
//					}
//				}
//			}
//			else {
//				moving = (topacc != Point(-1, -1) && point_list.size() < 10);
//				if (point_list.size() > 9) {
//					initialized = false;
//					lost = true;
//					cout << "Long hand gestures : Hand color reinitialized" << endl;
//				}
//			}
//
//			/// Show the relevant point of the movement
//			if (moving) {
//				steady_time = 0;
//				point_list.push_back(topacc);
//				circle(input_frame, topacc, 5, Scalar(0, 0, 255), CV_FILLED);
//			}
//			else {
//				/// If there were points listed in the barycenter list, this is an end of a movement
//				bool moved = movement(point_list, frame, input_frame, move_start);
//				//bool moved = false;
//				if (!lost && moved) {
//					lost = true;
//					cout << "Hand position recognition ended because you moved" << endl;
//				}
//				point_list = {};
//			}
//		}
//
//
//		/* --- POSE --- */
//
//
//		if (lost) {
//			initiation = false;
//			cut_validated = false;
//		}
//
//		if (initiation)
//			rectangle(input_frame, input_zone, Scalar(0, 0, 255), 3);
//
//		/// If validated mode or Enter is pressed, process graph cuts
//		if (cut_validated || initiation) {
//			/// If Enter has been pressed, calculate color averages in and out of the zone
//
//			if (initiation)
//				initAvgs(ycc_frame, input_zone, hand_avgs, out_hand_avgs);
//
//			/// Make graph cut
//			Mat grad, orient;
//			derivate(gray_frame, grad, orient);
//			if (initiation)
//				cuts = gCuts(ycc_frame, grad, hand_avgs, out_hand_avgs);
//			else {
//				Rect roi = getBoundingRect(ycc_frame, hand_contours, br_fact);
//				cuts = gCutsRoi(ycc_frame, grad, roi, hand_avgs, out_hand_avgs);
//			}
//
//			/// Remove contours in cuts to separate components
//			//removeContours(gray_frame, cuts);
//			//imshow(control_window, cuts);
//
//			/// Get connected components from the zone (from input zone to previous hand convex hull)
//			if (initiation)
//				initConnectedComponents(cuts, input_zone, final_cuts);
//			else
//				getConnectedComponents(cuts, final_cuts);
//
//			getContours(final_cuts, hand_contours);
//			if (lost) {
//				imshow(control_window, final_cuts);
//				continue;
//			}
//
//			//hullNDefects(cuts_to_show, hand_contours);
//			imshow(control_window, final_cuts);
//
//			/// Make predictions on pred_length consecutive frames
//			if (svm_enabled) {
//				int pose = svmPredict(svm, extractRoi(final_cuts, hand_contours));
//				if (predictions.size() != pred_length)
//					predictions.push_back(pose);
//				else {
//					int max = 0, dominant_pose = predictions[0];
//					for (int i = 0; i < predictions.size(); i++) {
//						int co = (int)count(predictions.begin(), predictions.end(), predictions[i]);
//						if (co > max) {
//							max = co;
//							dominant_pose = predictions[i];
//						}
//					}
//
//					if (prev_pose != -1 && prev_pose != dominant_pose)
//						cout << "Your hand position is " << training_sets[dominant_pose] << endl;
//					prev_pose = pose;
//
//					predictions.clear();
//				}
//			}
//
//			/// Compute new colors
//			//computeAvgs(ycc_frame, final_cuts, hand_avgs, out_hand_avgs);
//			Rect roi = getBoundingRect(ycc_frame, hand_contours, br_fact);
//			computeAvgsRoi(ycc_frame, final_cuts, roi, hand_avgs, out_hand_avgs);
//
//			initiation = false;
//		}
//
//		imshow(input_result_window, input_frame);
//		imshow(gray_window, move_frame);
//		//imshow("YCC", ycc_frame);
//	}
//
//	cap.release();
//	return 0;
//}