# README

Hand Control (Nicolas ZHAO & Elie ORIOL) - INF552 Image Vision Learning

Development of a hand control system, able to recognize hand motion & hand poses.

This project is made using OpenCV on C++ and is built using CMake.