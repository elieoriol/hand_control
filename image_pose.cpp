/// OpenCv
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/objdetect/objdetect.hpp>
/// C++
#include <time.h>

#include "maxflow/graph.h"

using namespace cv::ml;
using namespace cv;
using namespace std;

/// Camera idx
int cap_idx = 0;

/// Windows
String input_window = "Hand Input";
String input_result_window = "Hand Input Result";
String control_window = "Graph Cut";
int width = 480, height = 360;

/// Canny parameters
int lowThreshold = 100;
int threshRatio = 2;
int kernel_size = 3;

/// Graph cut colors
double norm_color_dist = 18.,
norm_hand_dist = 8.;
vector<Scalar> hand_avgs, out_hand_avgs;

/// Contours & bounding rectangle majoration factor
vector<vector<Point>> hand_contours;
double br_fact = 1.2;

/// Parameters for contour area checks
Mat1b prev_cuts;
double area_sum = 0.;
double br_keep_lim = 1.05;
bool lost = false;
bool keep_hand_avgs = false;

/// Key booleans
bool initiation = false;
bool cut_validated = false;

/// Learning parameters (save)
int train_idx = 1473;
Size training_img_size(128, 128);
const int training_sets_count = 5;
String training_sets[training_sets_count] = { "joined", "open", "peace", "fist", "thumb_up" };
String training_set = "fist";
String classifier_path = "../classifierModel.yml";
bool save = false;

/// Learning parameters (prediction)
bool svm_enabled = true;
const int pred_length = 10;
vector<int> predictions;


void toYCC(const Mat& frame, Mat& ycc_frame) {
	cvtColor(frame, ycc_frame, CV_BGR2YCrCb);
	/*for (int i = 0; i < ycc_frame.rows; i++) {
	for (int j = 0; j < ycc_frame.cols; j++) {
	for (int c = 0; c < 3; c++)
	ycc_frame.at<Vec3b>(i, j)[c] = saturate_cast<uchar>(0.5*(ycc_frame.at<Vec3b>(i, j)[c]));
	}
	}*/
}

void derivate(const Mat& I, Mat& G, Mat& O, bool angleInDegrees = false)
{
	Mat Gx, Gy;
	Sobel(I, Gx, CV_32F, 1, 0);
	Sobel(I, Gy, CV_32F, 0, 1);
	cartToPolar(Gx, Gy, G, O, angleInDegrees);
}

double f(double x) {
	return 1 / (1 + x*x);
}

double closestColorNorm(const Scalar& color, const vector<Scalar>& colors_set) {
	if (colors_set.empty())
		return 0.;

	double diff = norm(color - colors_set[0]);

	for (Scalar c : colors_set) {
		double ldiff = norm(color - c);
		if (ldiff < diff)
			diff = ldiff;
	}

	return diff;
}

Mat1b gCuts(const Mat& I, const Mat& Grad, const vector<Scalar>& hand, const vector<Scalar>& out)
{
	int w = I.cols,
		h = I.rows;

	int nodes_count = w*h;
	Graph<double, double, double> g(nodes_count, 2 * nodes_count);
	g.add_node(nodes_count);
	for (int i = 0; i < w - 1; i++) {
		for (int j = 0; j < h - 1; j++) {
			int node_id = j*w + i;
			Scalar color = I.at<Vec3b>(j, i);
			double dp0 = closestColorNorm(color, hand),
				dp1 = closestColorNorm(color, out);
			g.add_tweights(node_id, dp1, dp0);

			if (i < w - 1) {
				double lambda = (f(Grad.at<float>(j, i)) + f(Grad.at<float>(j, i + 1))) / 2;
				g.add_edge(node_id, node_id + 1, lambda, lambda);
			}
			if (j < h - 1) {
				double lambda = (f(Grad.at<float>(j, i)) + f(Grad.at<float>(j + 1, i))) / 2;
				g.add_edge(node_id, node_id + w, lambda, lambda);
			}
		}
	}

	double flow = g.maxflow();
	Mat1b C = Mat1b::zeros(h, w);
	for (int i = 0; i < w - 1; i++) {
		for (int j = 0; j < h - 1; j++) {
			int node_id = j*w + i;
			if (g.what_segment(node_id) == Graph<double, double, double>::SOURCE)
				C.at<uchar>(j, i) = uchar(255);
		}
	}

	return C;
}

Mat1b gCutsRoi(const Mat& I, const Mat& Grad, const Rect& roi, const vector<Scalar>& hand, const vector<Scalar>& out)
{
	int w = roi.width,
		h = roi.height;

	int nodes_count = w*h;
	Graph<double, double, double> g(nodes_count, 2 * nodes_count);
	g.add_node(nodes_count);
	for (int i = 0; i < w - 1; i++) {
		for (int j = 0; j < h - 1; j++) {
			int node_id = j*w + i;
			Scalar color = I.at<Vec3b>(roi.y + j, roi.x + i);
			double dp0 = closestColorNorm(color, hand),
				dp1 = closestColorNorm(color, out);
			g.add_tweights(node_id, dp1, dp0);

			double lambda = (f(Grad.at<float>(roi.y + j, roi.x + i)) + f(Grad.at<float>(roi.y + j, roi.x + i + 1))) / 2;
			g.add_edge(node_id, node_id + 1, lambda, lambda);

			lambda = (f(Grad.at<float>(roi.y + j, roi.x + i)) + f(Grad.at<float>(roi.y + j + 1, roi.x + i))) / 2;
			g.add_edge(node_id, node_id + w, lambda, lambda);
		}
	}

	double flow = g.maxflow();
	Mat1b C = Mat1b::zeros(I.rows, I.cols);
	for (int i = 0; i < w - 1; i++) {
		for (int j = 0; j < h - 1; j++) {
			int node_id = j*w + i;
			if (g.what_segment(node_id) == Graph<double, double, double>::SOURCE)
				C.at<uchar>(roi.y + j, roi.x + i) = uchar(255);
		}
	}

	return C;
}

Scalar vecToScalar(const vector<double>& vec) {
	Scalar scal;
	for (int i = 0; i < 3; i++)
		scal[i] = uchar(vec[i]);

	return scal;
}

Scalar getDominantColor(Mat& hand) {
	Mat samples(hand.total(), 3, CV_32F);
	float *samples_ptr = samples.ptr<float>(0);
	for (int row = 0; row != hand.rows; ++row) {
		uchar *src_begin = hand.ptr<uchar>(row);
		uchar *src_end = src_begin + hand.cols * hand.channels();
		while (src_begin != src_end) {
			samples_ptr[0] = src_begin[0];
			samples_ptr[1] = src_begin[1];
			samples_ptr[2] = src_begin[2];
			samples_ptr += 3; src_begin += 3;
		}
	}

	int cluster_count = 5;
	Mat labels;
	int attempts = 5;
	Mat centers;
	kmeans(samples, cluster_count, labels,
		TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS,
			10, 0.01),
		attempts, KMEANS_PP_CENTERS, centers);

	vector<int> colors_count(cluster_count);

	for (int row = 0; row != hand.rows; row++) {
		int *labels_ptr = labels.ptr<int>(row * hand.cols);
		int *labels_end = labels_ptr + hand.cols;

		while (labels_ptr != labels_end) {
			int const cluster_idx = *labels_ptr;
			colors_count[cluster_idx]++;
			labels_ptr++;
		}
	}

	int max_idx = 0;
	for (int i = 1; i < cluster_count; i++) {
		if (colors_count[i] > colors_count[max_idx])
			max_idx = i;
	}

	int sec_max_idx = max_idx == 0 ? 1 : 0;
	for (int i = 0; i < cluster_count; i++) {
		if (i != max_idx && colors_count[i] > colors_count[sec_max_idx])
			sec_max_idx = i;
	}

	vector<double> dominant_color_vec;
	centers.row(max_idx).copyTo(dominant_color_vec);
	Scalar dominant_color = vecToScalar(dominant_color_vec);

	if (dominant_color[0] == 0 && dominant_color[1] == 0 && dominant_color[2] == 0) {
		centers.row(sec_max_idx).copyTo(dominant_color_vec);
		dominant_color = vecToScalar(dominant_color_vec);
	}

	return dominant_color;
}

Rect getBoundingRect(const Mat& img, const vector<vector<Point>>& hand_contours, double fact = 1.0) {
	vector<Point> full_contours;
	for (int i = 0; i < hand_contours.size(); i++)
		full_contours.insert(full_contours.end(), hand_contours[i].begin(), hand_contours[i].end());

	if (full_contours.empty())
		return Rect(Point(0, 0), Size(img.cols, img.rows));

	Rect bound_rect = boundingRect(Mat(full_contours));

	if (fact != 1.0) {
		int half_f_width = int(fact * bound_rect.width / 2.),
			half_f_height = int(fact * bound_rect.height / 2.);

		Point top_left(max(0, bound_rect.x - half_f_width), max(0, bound_rect.y - half_f_height)),
			bottom_right(min(img.cols - 1, bound_rect.x + bound_rect.width + half_f_width), min(img.rows - 1, bound_rect.y + bound_rect.height + half_f_height));

		bound_rect = Rect(top_left, bottom_right);
	}

	return bound_rect;
}

Mat extractRoi(const Mat& img, const vector<vector<Point>>& hand_contours, double fact = 1.0) {
	Rect bound_rect = getBoundingRect(img, hand_contours, fact);

	Mat roi = img(bound_rect);
	resize(roi, roi, training_img_size);

	return roi;
}

struct IncGenerator {
	int current_;
	IncGenerator(int start) : current_(start) {}
	int operator() () { return current_++; }
};

void initAvgs(const Mat& frame, const Rect& input_zone, vector<Scalar>& hand_avgs, vector<Scalar>& out_hand_avgs) {
	Mat1b out_zone_mask(frame.size(), uchar(255));
	rectangle(out_zone_mask, input_zone, uchar(0), CV_FILLED);

	Mat hand = frame(input_zone);
	Scalar dominant_color = getDominantColor(hand);


	//hand_avg = mean(hand);
	//hand_avgs.push_back(hand_avg);

	hand_avgs.clear();
	hand_avgs.push_back(dominant_color);
	out_hand_avgs.clear();

	/// Consider points of the frame in a pseudo-random order so that it does not consider the top colors first
	vector<int> shuffled_rows(input_zone.height), shuffled_cols(input_zone.width);
	IncGenerator g(0);
	std::generate(shuffled_rows.begin(), shuffled_rows.end(), g);
	std::generate(shuffled_cols.begin(), shuffled_cols.end(), g);
	std::random_shuffle(shuffled_rows.begin(), shuffled_rows.end());
	std::random_shuffle(shuffled_cols.begin(), shuffled_cols.end());

	for (vector<int>::iterator ip = shuffled_rows.begin(); ip != shuffled_rows.end(); ip++) {
		int i = *ip;
		for (vector<int>::iterator jp = shuffled_cols.begin(); jp != shuffled_cols.end(); jp++) {
			int j = *jp;

			Scalar color = frame.at<Vec3b>(i, j);
			if (hand_avgs.size() <= 5 && closestColorNorm(color, hand_avgs) > norm_hand_dist / 2 && closestColorNorm(color, hand_avgs) < norm_hand_dist)
				hand_avgs.push_back(color);
		}
	}

	shuffled_rows = vector<int>(frame.rows);
	shuffled_cols = vector<int>(frame.cols);
	std::generate(shuffled_rows.begin(), shuffled_rows.end(), g);
	std::generate(shuffled_cols.begin(), shuffled_cols.end(), g);
	std::random_shuffle(shuffled_rows.begin(), shuffled_rows.end());
	std::random_shuffle(shuffled_cols.begin(), shuffled_cols.end());

	for (vector<int>::iterator ip = shuffled_rows.begin(); ip != shuffled_rows.end(); ip++) {
		int i = *ip;
		for (vector<int>::iterator jp = shuffled_cols.begin(); jp != shuffled_cols.end(); jp++) {
			int j = *jp;

			Scalar color = frame.at<Vec3b>(i, j);
			if (!input_zone.contains(Point(j, i)) && closestColorNorm(color, hand_avgs) > norm_hand_dist && (out_hand_avgs.empty() || closestColorNorm(color, out_hand_avgs) > norm_color_dist))
				out_hand_avgs.push_back(color);
		}
	}
}

void computeAvgs(const Mat& frame, const Mat1b& final_cuts, vector<Scalar>& hand_avgs, vector<Scalar>& out_hand_avgs) {

	//hand_avg = mean(frame, final_cuts);
	//new_hand_avgs.push_back(hand_avg);

	/*Mat hand;
	frame.copyTo(hand, final_cuts);
	Scalar dominant_color = getDominantColor(extractRoi(hand, hand_contours));
	hand_avgs.push_back(dominant_color);*/

	if (keep_hand_avgs)
		return;

	out_hand_avgs.clear();

	vector<Scalar> new_hand_avgs;

	/// Consider points of the frame in a pseudo-random order so that it does not consider the top colors first
	vector<int> shuffled_rows(frame.rows), shuffled_cols(frame.cols);
	IncGenerator g(0);
	generate(shuffled_rows.begin(), shuffled_rows.end(), g);
	generate(shuffled_cols.begin(), shuffled_cols.end(), g);
	random_shuffle(shuffled_rows.begin(), shuffled_rows.end());
	random_shuffle(shuffled_cols.begin(), shuffled_cols.end());

	for (vector<int>::iterator ip = shuffled_rows.begin(); ip != shuffled_rows.end(); ip++) {
		int i = *ip;
		for (vector<int>::iterator jp = shuffled_cols.begin(); jp != shuffled_cols.end(); jp++) {
			int j = *jp;

			Scalar color = frame.at<Vec3b>(i, j);

			// Find hand averages from graph cuts mask
			if (final_cuts.at<uchar>(i, j) != uchar(0)) {
				if ((new_hand_avgs.empty() && closestColorNorm(color, hand_avgs) < norm_hand_dist) || (closestColorNorm(color, hand_avgs) < 2 * norm_hand_dist && closestColorNorm(color, new_hand_avgs) > norm_hand_dist && closestColorNorm(color, new_hand_avgs) < 2 * norm_hand_dist))
					new_hand_avgs.push_back(color);
			}
		}
	}

	if (!new_hand_avgs.empty())
		hand_avgs = vector<Scalar>(new_hand_avgs);

	for (vector<int>::iterator ip = shuffled_rows.begin(); ip != shuffled_rows.end(); ip++) {
		int i = *ip;
		for (vector<int>::iterator jp = shuffled_cols.begin(); jp != shuffled_cols.end(); jp++) {
			int j = *jp;

			Scalar color = frame.at<Vec3b>(i, j);

			// Find hand averages from graph cuts mask
			if (final_cuts.at<uchar>(i, j) == uchar(0)
				&& closestColorNorm(color, hand_avgs) > norm_hand_dist
				&& (out_hand_avgs.empty() || closestColorNorm(color, out_hand_avgs) > norm_color_dist))
				out_hand_avgs.push_back(color);
		}
	}
}

void computeAvgsRoi(const Mat& frame, const Mat1b& final_cuts, const Rect& roi, vector<Scalar>& hand_avgs, vector<Scalar>& out_hand_avgs) {

	//hand_avg = mean(frame, final_cuts);
	//new_hand_avgs.push_back(hand_avg);

	/*Mat hand;
	frame.copyTo(hand, final_cuts);
	Scalar dominant_color = getDominantColor(extractRoi(hand, hand_contours));
	hand_avgs.push_back(dominant_color);*/

	if (keep_hand_avgs) {
		return;
	}

	out_hand_avgs.clear();

	vector<Scalar> new_hand_avgs;

	/// Consider points of the frame in a pseudo-random order so that it does not consider the top colors first
	vector<int> shuffled_rows(roi.height), shuffled_cols(roi.width);
	IncGenerator g(0);
	generate(shuffled_rows.begin(), shuffled_rows.end(), g);
	generate(shuffled_cols.begin(), shuffled_cols.end(), g);
	random_shuffle(shuffled_rows.begin(), shuffled_rows.end());
	random_shuffle(shuffled_cols.begin(), shuffled_cols.end());

	for (vector<int>::iterator ip = shuffled_rows.begin(); ip != shuffled_rows.end(); ip++) {
		int i = *ip;
		for (vector<int>::iterator jp = shuffled_cols.begin(); jp != shuffled_cols.end(); jp++) {
			int j = *jp;

			Scalar color = frame.at<Vec3b>(i, j);

			// Find hand averages from graph cuts mask
			if (final_cuts.at<uchar>(i, j) != uchar(0)
				&& closestColorNorm(color, hand_avgs) < norm_hand_dist / 2
				&& (new_hand_avgs.empty() || (closestColorNorm(color, new_hand_avgs) > norm_hand_dist && closestColorNorm(color, new_hand_avgs) < 2 * norm_hand_dist)))
				new_hand_avgs.push_back(color);
		}
	}

	if (!new_hand_avgs.empty())
		hand_avgs = vector<Scalar>(new_hand_avgs);

	for (vector<int>::iterator ip = shuffled_rows.begin(); ip != shuffled_rows.end(); ip++) {
		int i = *ip;
		for (vector<int>::iterator jp = shuffled_cols.begin(); jp != shuffled_cols.end(); jp++) {
			int j = *jp;

			Scalar color = frame.at<Vec3b>(i, j);

			// Find hand averages from graph cuts mask
			if (final_cuts.at<uchar>(i, j) == uchar(0)
				&& closestColorNorm(color, hand_avgs) > norm_hand_dist
				&& (out_hand_avgs.empty() || closestColorNorm(color, out_hand_avgs) > norm_color_dist))
				out_hand_avgs.push_back(color);
		}
	}

	//cout << Mat(hand_avgs) << endl;
}

void removeContours(const Mat& frame, const Mat& cuts) {
	Mat1b edges;
	vector<vector<Point>> init_contours;
	Canny(frame, edges, lowThreshold, lowThreshold*threshRatio, kernel_size);
	findContours(edges, init_contours, noArray(), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
	for (int i = 0; i < init_contours.size(); i++)
		drawContours(cuts, init_contours, i, uchar(0), 3, 8, noArray(), 0, Point());
}

void initConnectedComponents(const Mat& cuts, const Rect& input_zone, Mat1b& final_cuts) {
	Mat labels(cuts.size(), CV_32S);
	int nLabels = connectedComponents(cuts, labels, 8);

	vector<int> zone_labels(nLabels);
	for (int i = input_zone.y; i < input_zone.y + input_zone.height; i++) {
		for (int j = input_zone.x; j < input_zone.x + input_zone.width; j++) {
			int label = labels.at<int>(i, j);
			zone_labels[label] = label == 0 ? 0 : 1;
		}
	}

	final_cuts.copyTo(prev_cuts);
	final_cuts = Mat1b::zeros(cuts.size());
	for (int i = 0; i < final_cuts.rows; i++) {
		for (int j = 0; j < final_cuts.cols; j++) {
			if (zone_labels[labels.at<int>(i, j)] == 1)
				final_cuts.at<uchar>(i, j) = uchar(255);
		}
	}
}

void getConnectedComponents(const Mat& cuts, Mat1b& final_cuts) {
	Mat labels(cuts.size(), CV_32S);
	int nLabels = connectedComponents(cuts, labels, 8);

	vector<int> zone_labels(nLabels);
	for (int i = 0; i < labels.rows; i++) {
		for (int j = 0; j < labels.cols; j++) {
			if (final_cuts.at<uchar>(i, j) == uchar(255)) {
				int label = labels.at<int>(i, j);
				zone_labels[label] = label == 0 ? 0 : 1;
			}
		}
	}

	final_cuts.copyTo(prev_cuts);
	final_cuts = Mat1b::zeros(cuts.size());
	for (int i = 0; i < final_cuts.rows; i++) {
		for (int j = 0; j < final_cuts.cols; j++) {
			if (zone_labels[labels.at<int>(i, j)] == 1)
				final_cuts.at<uchar>(i, j) = uchar(255);
		}
	}
}

void getContours(Mat1b& final_cuts, vector<vector<Point>>& hand_contours) {
	vector<vector<Point>> prev_contours(hand_contours);
	findContours(final_cuts, hand_contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
	double prev_area_sum = area_sum;
	area_sum = 0;
	vector<double> contour_areas;
	for (int k = 0; k < hand_contours.size(); k++) {
		contour_areas.push_back(contourArea(hand_contours[k]));
		area_sum += contour_areas[k];
	}

	double area_ratio = area_sum / prev_area_sum;

	Rect prev_br = getBoundingRect(final_cuts, prev_contours, br_keep_lim), br = getBoundingRect(final_cuts, hand_contours);
	bool in_prev_br = (prev_br.width * prev_br.height >= br.width * br.height);
	if (in_prev_br && ((keep_hand_avgs && area_ratio > 0.9) || area_ratio > 1.0))
		keep_hand_avgs = true;
	else
		keep_hand_avgs = false;

	if (!initiation && !in_prev_br) {
		lost = true;
		hand_contours = vector<vector<Point>>(prev_contours);
		area_sum = prev_area_sum;
		final_cuts = Mat1b::zeros(prev_cuts.size());
		prev_cuts.copyTo(final_cuts);
	}
	else if (lost)
		lost = false;
}

//void hullNDefects(Mat& cuts_to_show, vector<vector<Point>> hand_contours) {
//	/// Convex Hull
//	vector<Point> full_contours;
//	vector<vector<Point>> hull(1);
//	for (int i = 0; i < hand_contours.size(); i++)
//		full_contours.insert(full_contours.end(), hand_contours[i].begin(), hand_contours[i].end());
//	convexHull(Mat(full_contours), hull[0]);
//	drawContours(cuts_to_show, hull, 0, Scalar(0, 0, 255), 2, 8, noArray(), 0, Point());
//
//	/// Convexity defects
//	vector<int> hull_indices;
//	convexHull(full_contours, hull_indices, true);
//	vector<Vec4i> defects;
//	convexityDefects(full_contours, hull_indices, defects);
//	for (Vec4i v: defects)
//		arrowedLine(cuts_to_show, full_contours[v[0]], full_contours[v[2]], Scalar(0, 255, 0));
//}

/// LEARNING

void saveTrainingImg(const Mat& img, const vector<vector<Point>> hand_contours) {
	Mat roi = extractRoi(img, hand_contours);
	imwrite("../training_images/" + training_set + "/" + to_string(train_idx++) + ".jpg", roi);
}

Mat deskew(const Mat& I) {
	Moments M;
	if (I.channels() != 1) {
		Mat gray;
		cvtColor(I, gray, CV_BGR2GRAY);
		M = moments(gray, true);
	}
	else
		M = moments(I, true);
	if (abs(M.mu02) < 0.01)
		return I.clone();
	double skew = M.mu11 / M.mu02;
	Matx23d mtx(1, skew, -0.5*training_img_size.width*skew, 0, 1, 0);
	Mat deskewed;
	warpAffine(I, deskewed, mtx, training_img_size, CV_WARP_INVERSE_MAP | INTER_LINEAR);
	return deskewed;
}

HOGDescriptor hog(
	training_img_size, //winSize
	training_img_size / 2, //blocksize
	training_img_size / 4, //blockStride,
	training_img_size / 2, //cellSize,
	16, //nbins,
	1, //derivAper,
	-1, //winSigma,
	0, //histogramNormType,
	0.2, //L2HysThresh,
	1,//gammal correction,
	64,//nlevels=64
	1);//Use signed gradients

String loadPattern(String& folder) {
	return String("../training_images/" + folder + "/*.jpg");
}

vector<float> imgToHog(Mat& img) {
	vector<float> descriptors;
	hog.compute(deskew(img), descriptors);
	return descriptors;
}

void loadTrainTestHogs(vector<vector<float>>& train_hogs, vector<int>& train_labels, vector<vector<float>>& test_hogs, vector<int>& test_labels) {
	vector<String> fn;

	for (int i = 0; i < training_sets_count; i++) {
		glob(loadPattern(training_sets[i]), fn, true);
		for (size_t k = 0; k < fn.size(); k++) {
			Mat img = imread(fn[k]);
			if (img.empty())
				continue;

			if (k % 2 == 0) {
				Mat mirror_img(img.size(), img.type());
				flip(img, mirror_img, 0);
				train_hogs.push_back(imgToHog(img));
				train_hogs.push_back(imgToHog(mirror_img));
				train_labels.push_back(i);
				train_labels.push_back(i);
			}
			else {
				Mat mirror_img(img.size(), img.type());
				flip(img, mirror_img, 0);
				test_hogs.push_back(imgToHog(img));
				test_hogs.push_back(imgToHog(mirror_img));
				test_labels.push_back(i);
				test_labels.push_back(i);
			}
		}
	}
}

Mat asMat(const vector<vector<float>>& hogs) {
	int descriptor_size = hogs[0].size();
	Mat mat(hogs.size(), descriptor_size, CV_32F);

	for (int i = 0; i < hogs.size(); i++) {
		for (int j = 0; j < descriptor_size; j++)
			mat.at<float>(i, j) = hogs[i][j];
	}

	return mat;
}

Ptr<SVM> svmInit(float C, float gamma) {
	Ptr<SVM> svm = SVM::create();
	svm->setGamma(gamma);
	svm->setC(C);
	svm->setKernel(SVM::RBF);
	svm->setType(SVM::C_SVC);

	return svm;
}

void svmTrain(Ptr<SVM> svm, Mat& train_mat, vector<int>& train_labels) {
	Ptr<TrainData> td = TrainData::create(train_mat, ROW_SAMPLE, Mat(train_labels));
	svm->train(td);
	svm->save(classifier_path);
}

void svmLoad(Ptr<SVM> svm, String& path) {
	svm = Algorithm::load<SVM>(path);
}

int svmPredict(Ptr<SVM> svm, Mat& img) {
	vector<float> hog = imgToHog(img);
	/*Mat hog_mat(1, int(hog.size()), CV_32F);
	for (int i = 0; i < int(hog.size()); i++)
	hog_mat.at<float>(0, i) = hog[i];*/
	float pose = svm->predict(hog);
	return int(pose);
}

void svmPredictTest(Ptr<SVM> svm, const Mat& test, Mat& res) {
	svm->predict(test, res);
}

float svmEvaluate(const Mat& res, const vector<int>& labels) {
	int c = 0;
	for (int i = 0; i < res.rows; i++) {
		if (res.at<float>(i, 0) == float(labels[i]))
			c++;
	}
	return (float(c) / float(res.rows)) * 100;
}


int main()
{
	float C = 12.5, gamma = 0.5;
	Ptr<SVM> svm = svmInit(C, gamma);

	if (svm_enabled) {
		//svmLoad(svm, classifier_path);

		vector<vector<float>> train_hogs, test_hogs;
		vector<int> train_labels, test_labels;
		loadTrainTestHogs(train_hogs, train_labels, test_hogs, test_labels);
		Mat train_mat = asMat(train_hogs),
			test_mat = asMat(test_hogs);
		svmTrain(svm, train_mat, train_labels);

		Mat res;
		svmPredictTest(svm, test_mat, res);
		cout << "Percentage of success: " << svmEvaluate(res, test_labels) << "%" << endl;
	}

	/// Create windows
	namedWindow(input_window, CV_WINDOW_AUTOSIZE);
	namedWindow(input_result_window, CV_WINDOW_AUTOSIZE);
	namedWindow(control_window, CV_WINDOW_AUTOSIZE);

	/// Capture from webcam
	VideoCapture cap(cap_idx);
	if (!cap.isOpened()) {
		printf("Error loading video capture");
		return -1;
	}

	Mat frame;
	cap.read(frame);

	/// Initialize cuts with frame size
	resize(frame, frame, Size(width, height));
	Mat cuts = Mat::zeros(frame.size(), CV_8UC3);
	Mat1b final_cuts = Mat1b::zeros(cuts.size());

	/// Create rectangle input zone for hand
	int h_border = frame.cols / 3,
		v_border = frame.rows / 10;
	Point vertex(h_border, v_border),
		opposite_vertex(frame.cols - h_border, frame.rows - v_border);
	Rect input_zone(vertex, opposite_vertex);

	int key = waitKey(3);

	/// While space bar isn't pressed
	while (key != 32) {
		time_t start = clock();

		/// Exit if space bar is pressed
		key = waitKey(3);

		/// Enter/Exit validated mode
		if (key == 8)
			cut_validated = !cut_validated;
		else if (key == 13)
			initiation = true;
		else if (key == 116)
			save = !save;

		/// Grab new video frame from camera
		cap.read(frame);
		resize(frame, frame, Size(width, height));

		/// Show frame with zone in which to place hand
		Mat input_frame;
		frame.copyTo(input_frame);
		rectangle(input_frame, input_zone, Scalar(0, 0, 255), 3);
		imshow(input_window, cut_validated ? frame : input_frame);

		/// If validated mode or Enter is pressed, process graph cuts
		if (cut_validated || initiation) {

			/// Convert frame to grayscale
			Mat gray_frame;
			cvtColor(frame, gray_frame, CV_BGR2GRAY);

			/// Convert frame to ycc and desaturate it
			Mat ycc_frame;
			toYCC(frame, ycc_frame);

			/// If Enter has been pressed, calculate color averages in and out of the zone
			if (initiation) {
				lost = false;
				initAvgs(ycc_frame, input_zone, hand_avgs, out_hand_avgs);
			}

			/// Make graph cut
			Mat grad, orient;
			derivate(gray_frame, grad, orient);
			if (initiation)
				cuts = gCuts(ycc_frame, grad, hand_avgs, out_hand_avgs);
			else {
				Rect roi = getBoundingRect(ycc_frame, hand_contours, br_fact);
				cuts = gCutsRoi(ycc_frame, grad, roi, hand_avgs, out_hand_avgs);
			}

			/// Remove contours in cuts to separate components
			//removeContours(gray_frame, cuts);
			imshow(control_window, cuts);

			/// Get connected components from the zone (from input zone to previous hand convex hull)
			if (initiation)
				initConnectedComponents(cuts, input_zone, final_cuts);
			else
				getConnectedComponents(cuts, final_cuts);

			getContours(final_cuts, hand_contours);
			if (lost) {
				imshow(input_result_window, final_cuts);
				continue;
			}

			//hullNDefects(cuts_to_show, hand_contours);
			imshow(input_result_window, final_cuts);

			if (svm_enabled) {
				int pose = svmPredict(svm, extractRoi(final_cuts, hand_contours));
				if (predictions.size() != 10)
					predictions.push_back(pose);
				else {
					int max = 0, dominant_pose = predictions[0];
					for (int i = 0; i < predictions.size(); i++) {
						int co = (int)count(predictions.begin(), predictions.end(), predictions[i]);
						if (co > max) {
							max = co;
							dominant_pose = predictions[i];
						}
					}

					cout << "Prediction: " << training_sets[dominant_pose] << endl;

					predictions.clear();
				}
			}

			/// Compute new color averages
			//computeAvgs(ycc_frame, final_cuts, hand_avgs, out_hand_avgs);
			Rect roi = getBoundingRect(ycc_frame, hand_contours, br_fact);
			computeAvgsRoi(ycc_frame, final_cuts, roi, hand_avgs, out_hand_avgs);

			initiation = false;
		}

		if (save) {
			saveTrainingImg(final_cuts, hand_contours);
			cout << "Writing done in ../training_images/" << training_set << "/" << train_idx - 1 << ".jpg" << endl;
		}
	}

	cap.release();
	return 0;
}